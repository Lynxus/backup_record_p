/**
 * @author Adam brychta
 * @date 1 10 2018
 *
 */

#ifndef MAYA22_H_INCLUDED
#define MAYA22_H_INCLUDED

int maya22(int argc, char* argv[]);

#endif // MAYA22_H_INCLUDED