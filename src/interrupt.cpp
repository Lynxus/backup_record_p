/**
 * @author Adam brychta
 * @date 1 10 2018
 *
 */

#include "interrupt.h"

#include "globals.h"

void Interrupt::set_thread(std::thread *new_thread){
  this->thread = new_thread;
}

void Interrupt::lock_in_lock(){
  this->lock_in->lock();
}

void Interrupt::lock_in_unlock(){
  this->lock_in->unlock();
}

void Interrupt::lock_out_lock(){
  this->lock_out->lock();
}

void Interrupt::lock_out_unlock(){
  this->lock_out->unlock();
}

void Interrupt::join(){
  this->thread->join();
  delete this->thread;
  delete this->lock_in;
  delete this->lock_out;
  delete this->mutex_active;
  delete this->lock_thread;
}

Interrupt::Interrupt(int id, std::mutex *mutex, std::mutex *interrupt_service, int *return_id){
  this->id = id;
  this->shared_mutex = mutex;
  this->interrupt_service = interrupt_service;

  this->active = FALSE;
  this->mutex_active = new std::mutex();
  this->mutex_active->unlock();

  this->lock_thread = new std::mutex();
  this->lock_thread->unlock();

  this->return_id = return_id;

  this->ended = FALSE;

  this->lock_in = new std::mutex();
  lock_in_lock();
  this->lock_out = new std::mutex();
  lock_out_lock();
}

int Interrupt::is_active(){
  this->mutex_active->lock();
  int result = this->active;
  this->mutex_active->unlock();
  return result;
}

void Interrupt::activate_interrupt(){
  this->mutex_active->lock();
  this->active = TRUE;
  this->mutex_active->unlock();
  this->lock_in_unlock();
}

void Interrupt::disable_interrupt(){
  this->mutex_active->lock();
  this->active = FALSE;
  this->mutex_active->unlock();
}

void Interrupt::throw_interrupt(){
  // Pokud se nepodari zamknout mutex, vlakno je v likvidaci.
  if (this->lock_thread->try_lock()){
    this->interrupt_service->lock();
    (*this->return_id) = this->id;
    this->shared_mutex->unlock();
    this->lock_thread->unlock();
  }
}

void Interrupt::wait_for_interrupt(){
  this->shared_mutex->lock();
}

std::mutex *Interrupt::get_lock_thread(){
  return this->lock_thread;
}

int Interrupt::get_ended(){
  return this->ended;
}

void Interrupt::set_ended(int ended){
  this->ended = ended;
}

Interrupt::~Interrupt(){
  delete mutex_active;
}

Interrupt_handler::Interrupt_handler(){
  this->interrupt = new std::mutex();
  this->interrupt->lock();
  this->interrupt_service = new std::mutex();
  this->interrupt_service->unlock();
  this->return_id = -1;
}

void Interrupt_handler::create_new_interrupt(int id){
  Interrupt *new_interrupt = new Interrupt(id, this->interrupt, this->interrupt_service, &(this->return_id));
  this->interrupts.push_back(new_interrupt);
}

void Interrupt_handler::set_thread_for_last(std::thread *thread){
  this->interrupts.at(this->interrupts.size() - 1)->set_thread(thread);
}

Interrupt *Interrupt_handler::get_last_interrupt(){
  return this->interrupts.at(this->interrupts.size() - 1);
}

void Interrupt_handler::activate_interrupts(){
  int i;
  int size = this->interrupts.size();
  for (i = 0; i < size; i++){
    this->interrupts.at(i)->activate_interrupt();
  }
}

void Interrupt_handler::wait_for_interrupt(){
  interrupt->lock();
}

int Interrupt_handler::get_return_id(){
  return this->return_id;
}

void Interrupt_handler::allow_interrupt(){
  interrupt_service->unlock();
}

void Interrupt_handler::disable_interrupts(){
  int i;
  int size = this->interrupts.size();
  for (i = 0; i < size; i++){
    this->interrupts.at(i)->disable_interrupt();
  }
}

void Interrupt_handler::end_interrupts(){
  int interrupt_ended = 0;
  int size = this->interrupts.size();

  int i;
  Interrupt *temp;

  while (interrupt_ended != size){
    this->interrupt_service->unlock();
    for (i = 0; i < size; i++){
      temp = this->interrupts.at(i);
      if (temp->get_ended() == FALSE){
        if (temp->get_lock_thread()->try_lock()){
          temp->lock_out_lock();
          temp->lock_in_unlock();
          temp->join();
          interrupt_ended++;
          temp->set_ended(TRUE); // Ukonceno.
        }
      }
    }
  }

  for (i = 0; i < size; i++){
    delete this->interrupts.at(i);
  }

  delete this->interrupt;
  delete this->interrupt_service;
}
