#include "parser.h"

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string.h>

std::string get_before_equal(std::string str){
  std::string conf;

  for(int i=0; i < str.size(); i++)
  {
      if(str[i] == '='){
        break;
      }
      conf += str[i];
  }

  return conf;
}

std::string get_after_equal(std::string str){
  std::string conf;
  int write = 0;

  for(int i=0; i < str.size(); i++)
  {
    if (write){
      conf += str[i];
    }
    if(str[i] == '='){
      write = 1;
    }
  }

  return conf;
}

std::string parse_config(FILE *fd){
  std::string conf;
  std::string cmd;
  char * line = NULL;
    size_t len = 0;
    ssize_t read;

  std::string record;

  while ((read = getline(&line, &len, fd)) != -1) {
    std::string temp = line;

    conf = get_before_equal(temp);
    cmd = get_after_equal(temp);

    if (conf.compare("conf_record") == 0){
      record = cmd;
    }
  }

  return record;
}