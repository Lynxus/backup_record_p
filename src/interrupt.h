/**
 * @author Adam brychta
 * @date 1 10 2018
 *
 */

#ifndef INTERRUPT_H_INCLUDED
#define INTERRUPT_H_INCLUDED

#include <thread>
#include <queue>
#include <mutex>

enum {
  INTERRUPT_ON,
  INTERRUPT_OFF
};

class Interrupt_handler;

class Interrupt {
   public:
    Interrupt(int id, std::mutex *mutex, std::mutex *interrupt_service, int *return_id);
    ~Interrupt();
    void activate_interrupt();
    void disable_interrupt();
    void throw_interrupt();
    void wait_for_interrupt();
    int is_active();
    void set_thread(std::thread *new_thread);
    void join();
    void lock_in_lock();
    void lock_in_unlock();
    void lock_out_lock();
    void lock_out_unlock();
    int get_ended();
    void set_ended(int ended);
    std::mutex *get_lock_thread();
  private:
    int id;
    std::mutex *shared_mutex;
    std::mutex *interrupt_service;

    std::mutex *lock_thread;
    int *return_id;
    int ended;

    int active;
    std::mutex *mutex_active;
    std::mutex *lock_in;
    std::mutex *lock_out;
    std::thread *thread;
};


class Interrupt_handler {
  public:
    Interrupt_handler();
    void create_new_interrupt(int id);
    void set_thread_for_last(std::thread *thread);
    Interrupt *get_last_interrupt();
    void activate_interrupts();

    void wait_for_interrupt();
    int get_return_id();
    void allow_interrupt();

    void disable_interrupts();
    void end_interrupts();
    
    Interrupt *get_interupt(int id);
  private:
    std::mutex *interrupt;
    std::mutex *interrupt_service;
    int return_id;

    std::vector <Interrupt *> interrupts;
};

#endif // INTERRUPT_H_INCLUDED