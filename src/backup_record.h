#pragma once

#include <boost/thread.hpp>

class CAFDBackupRecord{
  public:
    CAFDBackupRecord();
    ~CAFDBackupRecord();

    ///< Create Instance of IAFDDevice
    void CreateInstance();
    std::string conf_record;
    void stop();
  private:
    enum SyncState {
      eIdle = 0,
      ePeriodicSync,
      eSyncOnFinish,
      eMountError
    } procSyncState;

    std::string recFileName;
    std::string prevFileName;
    std::string dateFolder;
    bool nameChanged;

    bool procThreadRunnig;

    boost::thread processController;
    boost::thread processSync;

    static void Record(CAFDBackupRecord *device);
    static void Sync(CAFDBackupRecord *device);
    void idle();
};
