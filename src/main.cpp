/**
 * @author Adam brychta
 * @date 1 10 2018
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <poll.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h> 
#include <csignal>

#include <wiringPi.h>

#include "interrupt.h"
#include "maya22.h"
#include "parser.h"
#include "backup_record.h"

const int BTRUE = 1;
const int BFALSE = 0;
const int TOGGLE = 2;

const int PIN_LED_RECORDING = 14;
const int PIN_LED_ERROR = 15;
const int PIN_BUTTON = 18;
const int PIN_RACK_BUTTON = 11;

CAFDBackupRecord *recorder = new CAFDBackupRecord();

void start(int *run){
  // Zapne se nahravani.
  *run = BTRUE;
  digitalWrite(PIN_LED_RECORDING, HIGH);
  recorder->CreateInstance();
}

void stop(int *run){
  *run = BFALSE;
  digitalWrite(PIN_LED_RECORDING, LOW);
  recorder->stop();
}

void start_stop(int CMD){
  static int run = BFALSE;
  system("sudo mount -a");

  if (TOGGLE == CMD){
    if (BTRUE == run){
      // Vypne se nahravani.
      stop(&run);
    } else if (BFALSE == run) {
      start(&run);
    }
  } else if (BTRUE == CMD){
    start(&run);
  } else if (BFALSE == CMD){
    stop(&run);
  }
}
#include <sys/statvfs.h>
long GetAvailableSpace(const char* path)
{
  struct statvfs stat;
  
  if (statvfs(path, &stat) != 0) {
    // error happens, just quits here
    return -1;
  }

  // the available size is f_bsize * f_bavail
  return stat.f_bsize * stat.f_bavail;
}

void check_netdrive_memory(Interrupt *interrupt){
  interrupt->lock_in_lock();

  while (BTRUE == interrupt->is_active()){
    int free_space = GetAvailableSpace("/home/pi/shared_mnt") / 1000000000;
    if (free_space < 1000000000){
      digitalWrite(PIN_LED_ERROR, HIGH);
    }else {
      digitalWrite(PIN_LED_ERROR, LOW);
    }
    delay(30000);
  }

  interrupt->lock_out_unlock();
  interrupt->lock_in_lock();
}

void button_interrupt(Interrupt *interrupt){
  interrupt->lock_in_lock();

  int prev_state = HIGH;
  //*result = BFALSE;

  while (BTRUE == interrupt->is_active()){
    if (HIGH == prev_state && LOW == digitalRead(PIN_BUTTON)){
      prev_state = LOW;
      /*if(*result){
        *result = BFALSE;
      } else {
        *result = BTRUE;
      }*/
      interrupt->throw_interrupt();
    } else if(LOW == prev_state && HIGH == digitalRead(PIN_BUTTON)){
      prev_state = HIGH;
    }
    delay(100);
  }

  interrupt->lock_out_unlock();
  interrupt->lock_in_lock();
}

void rack_button_interrupt(Interrupt *interrupt){
  interrupt->lock_in_lock();

  int prev_state = HIGH;
  //*result = BFALSE;

  while (BTRUE == interrupt->is_active()){
    if (HIGH == prev_state && LOW == digitalRead(PIN_RACK_BUTTON)){
      prev_state = LOW;
      /*if(*result){
        *result = BFALSE;
      } else {
        *result = BTRUE;
      }*/
      interrupt->throw_interrupt();
    } else if(LOW == prev_state && HIGH == digitalRead(PIN_RACK_BUTTON)){
      prev_state = HIGH;
    }
    delay(100);
  }

  interrupt->lock_out_unlock();
  interrupt->lock_in_lock();
}

enum{
  INT_BACKUP_BUTTON,
  INT_RACK_BUTTON,
  INT_DISK_RED,
  INT_KILL
};

Interrupt_handler *handler = new Interrupt_handler();

void kill(){
  digitalWrite(PIN_LED_RECORDING, LOW);
  digitalWrite(PIN_LED_ERROR, LOW);
  
  handler->disable_interrupts();
  handler->end_interrupts();

  delete handler;
}

void ctrl_c(int sig){
  sig++;

  kill();

  exit(0);
}

int main(/*int argc, char *argv[]*/){
  // Init.
  wiringPiSetupGpio();
  signal(SIGINT, ctrl_c);

  // GPIO PIN init.
  pinMode(PIN_LED_RECORDING, OUTPUT);
  pinMode(PIN_LED_ERROR, OUTPUT);
  pinMode(PIN_RACK_BUTTON, INPUT);
  pullUpDnControl(PIN_RACK_BUTTON, PUD_UP);
  pinMode(PIN_BUTTON, INPUT);
  pullUpDnControl(PIN_BUTTON, PUD_UP);
  // Default LED values.
  digitalWrite(PIN_LED_RECORDING, LOW);
  digitalWrite(PIN_LED_ERROR, LOW);

  int interrupt_id;

  //FILE *config_file;
  //config_file = fopen("/home/pi/backup_record/conf", "rb");
  //std::string record = parse_config(config_file);

  recorder->conf_record = ".wav trim 0 0:00:10";

  //maya22-hidraw -i mic_hiz -l 231 -r 231
  char *maya22_argv[7];
  char *name = (char *)"maya22-hidraw";
  char *par1 = (char *)"-i";
  char *par2 = (char *)"mic_hiz";
  char *par3 = (char *)"-l";
  char *par4 = (char *)"231";
  char *par5 = (char *)"-r";
  char *par6 = (char *)"231";
  maya22_argv[0] = name;
  maya22_argv[1] = par1;
  maya22_argv[2] = par2;
  maya22_argv[3] = par3;
  maya22_argv[4] = par4;
  maya22_argv[5] = par5;
  maya22_argv[6] = par6;
  int err;
  err = maya22(7, maya22_argv);
  while (0 != err){
    err = maya22(7, maya22_argv);
    delay(10000);
  }

  /*int button_result;*/

  

  // Interrupt settings.
  handler->create_new_interrupt(INT_BACKUP_BUTTON);
  handler->set_thread_for_last(new std::thread(button_interrupt, handler->get_last_interrupt()));
  handler->create_new_interrupt(INT_RACK_BUTTON);
  handler->set_thread_for_last(new std::thread(rack_button_interrupt, handler->get_last_interrupt()));
  handler->activate_interrupts();

  // Main loop.
  while (1){
    handler->wait_for_interrupt();

    interrupt_id = handler->get_return_id();
    printf("%d\n", interrupt_id);
    switch (interrupt_id) {
      case INT_BACKUP_BUTTON:
        start_stop(TOGGLE);
        break;
      case INT_RACK_BUTTON:
        start_stop(TOGGLE);
        break;
      case INT_DISK_RED:
        break;
      case INT_KILL:
        break;
      default:
        break;
    }
    
    handler->allow_interrupt();
  }

  kill();

  return 0;
}