/**
 * @author Adam brychta
 * @date 1 10 2018
 *
 */

#include "backup_record.h"

#include <sys/mount.h>

#define LOCAL           "/home/pi/mytmp"
#define SMB_MNT   "/home/pi/shared_mnt"

#define AUDIO_DEVICE    "AUDIODEV=hw:1,0 AUDIODRIVER=alsa "
#define BACKUP_INTERVAL 3000   //3 seconds

#define RECORD_AUDIO    AUDIO_DEVICE "rec " SMB_MNT "/%s/%s%s"

CAFDBackupRecord::CAFDBackupRecord() {

}


CAFDBackupRecord::~CAFDBackupRecord() {

}

void CAFDBackupRecord::stop(){
  this->procThreadRunnig = false;
}

void CAFDBackupRecord::CreateInstance() {

  this->recFileName = "rec";
  this->dateFolder = "date";

  //start main idle
  procThreadRunnig = true;
  procSyncState = eIdle;

  processController = boost::thread(&CAFDBackupRecord::Record, this);
  processSync = boost::thread(&CAFDBackupRecord::Sync, this);

}

void CAFDBackupRecord::Record(CAFDBackupRecord *device) {
  boost::posix_time::milliseconds sleepTime(20);

  time_t now;                         //timestamp
  char recordCmd[200];                //command used for recording
  char timestamp[40];                      //string formated date
  char date[10];

  system("mkdir -p " LOCAL);

  while (device->procThreadRunnig) {

    //get human readable date
    now = time (0); //get current date
    strftime (timestamp, 20, "%Y-%m-%d_%H:%M:%S", localtime (&now));
    /*device->recFileName.insert(device->recFileName.begin(), timestamp,
                               timestamp + 20);*/
    /*device->dateFolder.insert(device->dateFolder.begin(), timestamp,
                              timestamp + 10);*/
    sprintf(recordCmd, RECORD_AUDIO, device->dateFolder.data(), timestamp,
            device->conf_record.c_str());

    char mkdir[100];
    sprintf(mkdir, "mkdir -p " LOCAL "/%s", device->dateFolder.data());
    system(mkdir);

    //record audio and continuous sync
    device->procSyncState = ePeriodicSync;
    std::cout << "------------Recording audio------------" << std::endl;
    system(recordCmd);  //record audio

    //sync after record finished (check ssh connection)
    device->procSyncState = eSyncOnFinish;

    //go to sleep (sleep less if processing is more demanding)
    clock_t begin = clock();
    device->idle();
    clock_t end = clock();
    double elapsed_msecs = double(end - begin);
    if (elapsed_msecs < 20) {
      boost::posix_time::milliseconds c_sleepTime(20 - elapsed_msecs);
      boost::this_thread::sleep(c_sleepTime);
    }
  }
}

void CAFDBackupRecord::Sync(CAFDBackupRecord *device) {

  boost::posix_time::milliseconds sleepTime(BACKUP_INTERVAL);
  bool mounted = 1;

  while (device->procThreadRunnig) {

    //if mounted
    /*if(system("grep '" SMB_MNT "' /proc/mounts > /dev/null") != 0) {
        device->procSyncState = eMountError;
        mounted = 0;
    }else {
        mounted = 1;
    }*/

    //sync only if its recording
    
    switch(device->procSyncState) {
    case eIdle: { //do nothing
      std::cout << "------------Sync idle------------" << std::endl;
      break;
    }
    case ePeriodicSync: { //sync while recording
      std::cout << "------------Sync periodic------------" << std::endl;
      if(mounted)
        system("rsync -a --append " LOCAL "/ " SMB_MNT);

      break;
    }
    case eSyncOnFinish: { //after recording stop. Check the mount. If it is OK finally sync and delete
      std::cout << "------------Sync finish------------" << std::endl;
      if(mounted) {
        system("rsync -a --append " LOCAL "/ " SMB_MNT);
        system("rm -rf " LOCAL "/*");
        if(device->nameChanged) {
          char moveStr[100];
          sprintf(moveStr, "mv " SMB_MNT "%1$s/%2$s " SMB_MNT "%1$s/%3$s ",
                  device->dateFolder.data(), device->prevFileName.data(),
                  device->recFileName.data());
          system(moveStr);
        }
      }

      device->procSyncState = eIdle;
      break;
    }
    case eMountError: { //mounted drive is not reachable. Send message
      std::cout << "-----Network drive unavailable-----" << std::endl;
      break;
    }
    default: {
      perror("Unknown sync command");
      exit(0);
    }
    }



    //go to sleep (sleep less if processing is more demanding)
    clock_t begin = clock();
    device->idle();
    clock_t end = clock();
    double elapsed_msecs = double(end - begin);
    if (elapsed_msecs < BACKUP_INTERVAL) {
      boost::posix_time::milliseconds c_sleepTime(BACKUP_INTERVAL - elapsed_msecs);
      boost::this_thread::sleep(c_sleepTime);
    }

  }
}

void CAFDBackupRecord::idle() {
  //messagesQueue

}
