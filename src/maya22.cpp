/**
 * @author Adam brychta
 * @date 1 10 2018
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <getopt.h>
#include <hidapi/hidapi.h>

/* USB ID */
#define VENDOR_ID	0x2573
#define PRODUCT_ID	0x0017

#define CHIP_ADDRESS	0x34

/* I2C commands */
#define CMD_UNMUTE	(0x0D << 1)		/* Unmute Headphone */
#define CMD_VOL_OUT_L	((0x3 << 1) + 1)	/* Set Volume OUT L */
#define CMD_VOL_OUT_R	((0x4 << 1) + 1)	/* Set Volume OUT R */
#define CMD_VOL_IN_L	(0xE << 1)		/* Set Volume IN L */
#define CMD_VOL_IN_R	(0xF << 1)		/* Set Volume IN R */
#define CMD_IN_SEL	(0x15 << 1)		/* Set Input Select and
						   input MUTE */

#define HEADPHONE_UMUTE	(0)

#define OUT_MIN		(110)
#define OUT_MAX		(OUT_MIN + 145)

#define IN_MIN		(104)
#define IN_MAX		(IN_MIN + 127)

#define INPUT_MIC	(0x01u)
#define INPUT_HIZ	(0x02u)
#define INPUT_LINE	(0x04u)
#define INPUT_MIC_HIZ	(0x08u)
#define INPUT_MUTE	(0xC0u)

#define INPUT_CLR_MASK	(0xF0)

typedef struct {
	uint8_t cmd;
	uint8_t data;
	bool used;
} cmd_t;

typedef enum {
	E_CMD_UNMUTE = 0,
	E_CMD_VOL_OUT_L,
	E_CMD_VOL_OUT_R,
	E_CMD_VOL_IN_L,
	E_CMD_VOL_IN_R,
	E_CMD_IN_SEL,
	E_CMD_LAST,
} e_cmd;

typedef struct {
	uint8_t	ucI2cCtrl;
	uint8_t	ucI2cDevId;
	uint8_t	usI2cSubAddrL;
	uint8_t	usI2cSubAddrH;
	uint8_t	ucByteCnt;
	uint8_t	pucData[16];
	uint8_t	ucI2cCtrlReg;
} usb_i2c_data_t;

static void usage(void)
{
	printf("maya22-hid usage:\n" \
		"-i <SELECT>\tinput select\n" \
		"\tmic\n\thiz\n\tline\n\tmic_hiz\n" \
		"-m\tmute input\n" \
		"-L <%u;%u>\tvolume out left\n" \
		"-R <%u;%u>\tvolume out right\n" \
		"-l <%u;%u>\tvolume in left\n" \
		"-r <%u;%u>\tvolume in right\n" \
		"-u\tunmute headphone\n" \
		"-h\tdisplay this help and exit\n",
		OUT_MIN, OUT_MAX, OUT_MIN, OUT_MAX,
		IN_MIN,  IN_MAX,  IN_MIN,  IN_MAX);
}

static int args_parse(int argc, char * const argv[], cmd_t * commands)
{
	int c;
	int volume;
	e_cmd volume_type;

	while ((c = getopt(argc, argv, "i:mL:R:l:r:uh")) != -1) {
		switch (c) {
		case 'i':
			commands[E_CMD_IN_SEL].cmd = CMD_IN_SEL;
			commands[E_CMD_IN_SEL].data &= INPUT_CLR_MASK;
			commands[E_CMD_IN_SEL].used = true;

			if (strcmp("mic", optarg) == 0) {
				commands[E_CMD_IN_SEL].data |= INPUT_MIC;
			}
			else if (strcmp("hiz", optarg) == 0) {
				commands[E_CMD_IN_SEL].data |= INPUT_HIZ;
			}
			else if (strcmp("line", optarg) == 0) {
				commands[E_CMD_IN_SEL].data |= INPUT_LINE;
			}
			else if (strcmp("mic_hiz", optarg) == 0) {
				commands[E_CMD_IN_SEL].data |= INPUT_MIC_HIZ;
			}
			else {
				usage();
				return -1;
			}
			break;
		case 'm':
			commands[E_CMD_IN_SEL].cmd = CMD_IN_SEL;
			commands[E_CMD_IN_SEL].data |= INPUT_MUTE;
			commands[E_CMD_IN_SEL].used = true;
			break;
		case 'L':
		case 'R':
			volume_type = (c == 'L') ?
				E_CMD_VOL_OUT_L : E_CMD_VOL_OUT_R;
			volume = atoi(optarg);
			if (volume > OUT_MAX || volume < OUT_MIN) {
				usage();
				return -1;
			}

			commands[volume_type].cmd = (c == 'L') ?
				CMD_VOL_OUT_L : CMD_VOL_OUT_R; 
			commands[volume_type].data = (uint8_t) volume;
			commands[volume_type].used = true;
			break;
		case 'l':
		case 'r':
			volume_type = (c == 'l') ?
				E_CMD_VOL_IN_L : E_CMD_VOL_IN_R;
			volume = atoi(optarg);
			if (volume > IN_MAX || volume < IN_MIN) {
				usage();
				return -1;
			}

			commands[volume_type].cmd = (c == 'l') ?
				CMD_VOL_IN_L : CMD_VOL_IN_R; 
			commands[volume_type].data = (uint8_t) volume;
			commands[volume_type].used = true;
			break;
		case 'u':
			commands[E_CMD_UNMUTE].cmd = CMD_UNMUTE;
			commands[E_CMD_UNMUTE].data = HEADPHONE_UMUTE;
			commands[E_CMD_UNMUTE].used = true;
			break;
		case 'h':
		case '?':
			usage();
			return -1;
		}
	}

	if (optind < argc) {
		usage();
		return -1;
	}

	return 0;
}

static int write_i2c(hid_device *dev, usb_i2c_data_t *p_i2c_data)
{
	uint8_t ucOutReport[32];

	memset(&(ucOutReport[0]), 0, 32);

	ucOutReport[0] = (uint8_t) 0;
	ucOutReport[1] = p_i2c_data->ucI2cCtrl;
	ucOutReport[2] = p_i2c_data->ucI2cDevId;
    
	ucOutReport[3] = p_i2c_data->usI2cSubAddrL;
	ucOutReport[4] = p_i2c_data->usI2cSubAddrH;
	ucOutReport[5] = p_i2c_data->ucByteCnt;
	if (p_i2c_data->pucData)
		memcpy(&ucOutReport[6], p_i2c_data->pucData,
		       p_i2c_data->ucByteCnt);

	ucOutReport[22] = (uint8_t) 0x80;
	
	return hid_write(dev, ucOutReport, 32);
}

static int set_i2c_data(hid_device *dev, uint8_t sub_add, uint8_t data)
{
	usb_i2c_data_t i2c_data;

	if (!dev)
		return -1;

	memset(&i2c_data, 0, sizeof(i2c_data));
	i2c_data.ucI2cCtrl = 0x12;
	i2c_data.ucI2cDevId = CHIP_ADDRESS;
	i2c_data.usI2cSubAddrH = 0;
	i2c_data.usI2cSubAddrL = sub_add;
	i2c_data.ucByteCnt = 1;
	i2c_data.pucData[0] = data;
	
	return (write_i2c(dev, &i2c_data) < 0) ? -1 : 0;
}

int maya22(int argc, char* argv[])
{
	opterr = 0; /* disable error messages by getopt() */
	hid_device *handle;
	cmd_t commands[E_CMD_LAST] = { 0 };
	int res;

	if (args_parse(argc, argv, commands))
		return -1;
	
	if (hid_init()) {
		fprintf(stderr, "HID API initialization failed\n");
		return -1;
	}
	if ((handle = hid_open(VENDOR_ID, PRODUCT_ID, NULL)) == NULL) {
		fprintf(stderr, "Unable to open HID device\n");
		hid_exit();
 		return -1;
	}

	for (int cmd = 0; cmd < (int)E_CMD_LAST; cmd++) {
		if (commands[cmd].used) {
			res = set_i2c_data(handle, commands[cmd].cmd,
					   commands[cmd].data);

			if (res)
				fprintf(stderr, "HID: I2C command %x failed " \
					"%d\n", commands[cmd].cmd, res);
		}
	}

	hid_close(handle);
	hid_exit();

	return 0;
}

