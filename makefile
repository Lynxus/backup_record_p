CC=g++
CXXFLAGS=-std=c++14 -pedantic -Wall -Wextra -pthread -lhidapi-hidraw -lstdc++ -lboost_thread -lboost_system
LDFLAGS	=-l wiringPi 
exe=backup_rec

SRC_DIR=src/
BUILD=obj/
OBJFILES=obj/main.o obj/interrupt.o obj/maya22.o obj/backup_record.o obj/parser.o

$(BUILD)%.o : $(SRC_DIR)%.cpp
	$(CC) $(CXXFLAGS) -c $< -o $@

all: $(exe)

$(exe): $(OBJFILES)
	$(CC) $(CXXFLAGS)  $(OBJFILES) -o $@ $(LDFLAGS)

# clean binary and .o files
cl: clean-exe clo

clean-exe:
	rm -f $(exe)

clo:
	rm -f $(BUILD)*.o core